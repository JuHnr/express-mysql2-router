const express = require("express");
const router = express.Router();
// connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db");

/*Routes pour le CRUD */

//route GET pour récupérer toutes les voitures de la base de données
router.get('/', (req, res) => {
  connection.query('SELECT * FROM garage', (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la récupération des garages');
    } else {
      res.json(results);
    }
  });
});

// Route GET pour récupérer un garage par son id
router.get('/:id', (req, res) => {
  const id = req.params.id;
  connection.execute('SELECT * FROM garage WHERE garage_id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la récupération du garage');
    } else {
      res.json(results);
    }
  });
});

// Route POST pour ajouter un nouveau garage dans la base de données
router.post('/', (req, res) => {
  const { name, email } = req.body;
  connection.execute(
    "INSERT INTO garage (name, email) VALUES (?, ?)",
    [name, email],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout du garage");
      } else {
        res.status(201).send(`Garage ajouté avec l'ID ${results.insertId}`);
      }
    }
  );
});

// Route PUT pour mettre à jour par  ID
router.put('/:id', (req, res) => {
  const { name, email } = req.body;
  const id = req.params.id;
  connection.execute(
    "UPDATE garage SET name = ?, email = ? WHERE garage_id = ?",
    [name, email, id],
    (err, result) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour du garage");
      } else {
        res.send(`Garage mis à jour.`);
      }
    }
  );
});

// Route DELETE pour supprimer par ID
router.delete('/:id', (req, res) => {
  const id = req.params.id;
  connection.execute('DELETE FROM garage WHERE garage_id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la suppression du garage');
    } else {
      res.send(`Garage supprimé.`);
    }
  });
});


module.exports = router;
