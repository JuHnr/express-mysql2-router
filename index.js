// importe le module Express
const express = require("express");
require("dotenv").config();
//déplacement de connection dans cars.controller.js

// import de la bibiothèque cors
const cors = require("cors");


const app = express();

// Configure l'application pour parser les requêtes avec des corps au format JSON.
app.use(express.json());

// Configure l'application pour parser les requêtes avec des corps encodés en URL (utile pour les formulaires web).
app.use(express.urlencoded({extended: true,}));

// utilisation du port 3000 pour accéder au serveur
const port = 3000;

// ajout d'une configuration pour gérer les CORS 
app.use(
  cors({
    origin: "http://localhost:8080",
  })
);

// Importe le contrôleur pour les voitures
const carsController = require('./cars.controller');

// Utilise le contrôleur pour gérer les routes des voitures, crée l'url racine localhost:3000/cars
app.use('/cars', carsController);

// Importe le contrôleur pour le garage
const garagesController = require('./garages.controller');

// Utilise le contrôleur pour gérer les routes des voitures, crée l'url racine localhost:3000/garages
app.use('/garages', garagesController);

// Démarre le serveur Express et écoute sur le port spécifié
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});